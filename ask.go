// Copyright 2017 Pantomath SAS
// Copyright 2021 Charles Francoise
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package ask collects information from the user through the command line in a Q&A style.
//
//
package ask

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/peterh/liner"
)

// StringAsker is an object that can ask for a string on the command line.
type StringAsker struct {
	format        string
	args          []interface{}
	defaultString string
	hasDefault    bool
	password      bool
	choices       []string
	completer     liner.Completer
}

// Ask prompts the user by displaying the StringAsker's message, reads from standard input until it encounters a newline
// or EOF and returns the input string. If the user enters an empty input and a default string is set, the default
// string will be returned.
// Returns an error if stdin could not be read, or if the input is too large for memory.
func (sa *StringAsker) Ask() (string, error) {
	return sa.doAsk(sa.getPrompt())
}

func (sa *StringAsker) getPrompt() string {
	var b strings.Builder
	// Output prompt to Stdout
	fmt.Fprintf(&b, sa.format+" ", sa.args...)
	if len(sa.choices) > 0 {
		fmt.Fprintf(&b, "[%s] ", strings.Join(sa.choices, "/"))
		if sa.hasDefault {
			fmt.Fprintf(&b, "(default=%s) ", sa.defaultString)
		}
	} else if sa.hasDefault {
		fmt.Fprintf(&b, "[%s] ", sa.defaultString)
	}

	return b.String()
}

func (sa *StringAsker) doAsk(prompt string) (string, error) {
	// Read from Stdin
	l := liner.NewLiner()
	defer l.Close()
	l.SetCtrlCAborts(true)
	l.SetTabCompletionStyle(liner.TabPrints)
	if len(sa.choices) > 0 {
		// Set auto-completion from choices
		l.SetCompleter(func(line string) []string {
			completions := make([]string, 0, len(sa.choices))
			for _, c := range sa.choices {
				if strings.HasPrefix(c, line) {
					completions = append(completions, c)
				}
			}
			return completions
		})
	} else {
		l.SetCompleter(sa.completer)
	}

	var s string
	var err error
	if sa.password {
		s, err = l.PasswordPrompt(prompt)
		if err != nil {
			return "", fmt.Errorf("ask: error reading user input: %w", err)
		}
	} else {
		s, err = l.Prompt(prompt)
		if err != nil {
			return "", fmt.Errorf("ask: error reading user input: %w", err)
		}
	}

	// If input is empty, return default
	if len(s) == 0 && sa.hasDefault {
		s = sa.defaultString
	}

	// Validate against available choices
	if len(sa.choices) > 0 {
		for _, c := range sa.choices {
			if c == s {
				return c, nil
			}
		}
		return "", fmt.Errorf("%s is not a valid choice", s)
	}

	return s, err
}

// Default sets the default string for the StringAsker. The default string is returned from Ask if the user entered no
// input. The default value will be automatically displayed when prompting the user. If the default is not in the set of
// available choices, behavior is undefined (see StringAsker.Choices).
func (sa *StringAsker) Default(def string) *StringAsker {
	sa.defaultString = def
	sa.hasDefault = true
	return sa
}

// Password sets the password flag for the StringAsker. If password flag is set, the Prompt will not display user input.
func (sa *StringAsker) Password() *StringAsker {
	sa.password = true
	return sa
}

// Choices sets an arbitrary number of choices for the user input. The list of choices will be automatically displayed
// when prompting the user and choices will be available to auto-completion using TAB in the readline style. If the user
// enters a choice not in the list, Ask will return an error. If the set of available "choices" does not contain the
// default, behavior is undefined.
func (sa *StringAsker) Choices(choices ...string) *StringAsker {
	sa.choices = make([]string, len(choices))
	copy(sa.choices, choices)
	return sa
}

// String returns a StringAsker that will prompt the user with message when Ask is called.
func String(message string) *StringAsker { return Stringf(message) }

// Stringf returns a StringAsker that will prompt the user with a message according to a format specifier. See the fmt
// package for formatting rules.
func Stringf(format string, args ...interface{}) *StringAsker {
	return &StringAsker{
		format: format,
		args:   args,
	}
}

// IntAsker is an object that can ask for an int on the command line.
type IntAsker struct {
	sa *StringAsker
}

// Ask prompts the user by displaying the IntAsker's message, reads from standard input until it encounters a newline
// or EOF, converts the input to an int and returns the int. If the user enters an empty input and a default
// int is set, the default int will be returned.
// Returns an error if stdin could not be read, if the input is too large for memory, or if the input could not be
// converted to memory.
func (ia *IntAsker) Ask() (int, error) {
	s, err := ia.sa.Ask()
	if err != nil {
		return 0, err
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		return 0, fmt.Errorf("ask: error parsing int from user input: %w", err)
	}
	return i, nil
}

// Default sets the default int for the IntAsker. The default int is returned from Ask if the user entered
// an empty input.
func (ia *IntAsker) Default(def int) *IntAsker {
	ia.sa = ia.sa.Default(strconv.Itoa(def))
	return ia
}

// Choices sets an arbitrary number of choices for the user input. The list of choices will be automatically displayed
// when prompting the user. If the user enters a choice not in the list, Ask will return an error. If the set of
// available "choices" does not contain the default, behavior is undefined.
func (ia *IntAsker) Choices(choices ...int) *IntAsker {
	stringChoices := make([]string, len(choices))
	for i, n := range choices {
		stringChoices[i] = strconv.Itoa(n)
	}
	ia.sa.Choices(stringChoices...)
	return ia
}

// Int returns an IntAsker that will prompt the user with message when Ask is called.
func Int(message string) *IntAsker { return Intf(message) }

// Intf returns an IntAsker that will prompt the user with a message according to a format specifier. See the fmt
// package for formatting rules.
func Intf(format string, args ...interface{}) *IntAsker {
	return &IntAsker{
		sa: Stringf(format, args...),
	}
}

// FloatAsker is an object that can ask for a float on the command line.
type FloatAsker struct {
	sa *StringAsker
}

// Ask prompts the user by displaying the FloatAsker's message, reads from standard input until it encounters a newline
// or EOF, converts the input to a float and returns the float. If the user enters an empty input and a default
// float is set, the default float will be returned.
// Returns an error if stdin could not be read, if the input is too large for memory, or if the input could not be
// converted to memory.
func (fa *FloatAsker) Ask() (float64, error) {
	s, err := fa.sa.Ask()
	if err != nil {
		return 0, err
	}
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, fmt.Errorf("ask: error parsing float from user input: %w", err)
	}
	return f, nil
}

// Default sets the default float for the FloatAsker. The default float is returned from Ask if the user entered
// an empty input.
func (fa *FloatAsker) Default(def float64) *FloatAsker {
	fa.sa = fa.sa.Default(strconv.FormatFloat(def, 'f', -1, 64))
	return fa
}

// Choices sets an arbitrary number of choices for the user input. The list of choices will be automatically displayed
// when prompting the user. If the user enters a choice not in the list, Ask will return an error. If the set of
// available "choices" does not contain the default, behavior is undefined.
func (fa *FloatAsker) Choices(choices ...float64) *FloatAsker {
	stringChoices := make([]string, len(choices))
	for i, f := range choices {
		stringChoices[i] = strconv.FormatFloat(f, 'f', -1, 64)
	}
	fa.sa.Choices(stringChoices...)
	return fa
}

// Float returns an FloatAsker that will prompt the user with message when Ask is called.
func Float(message string) *FloatAsker { return Floatf(message) }

// Floatf returns an FloatAsker that will prompt the user with a message according to a format specifier. See the fmt
// package for formatting rules.
func Floatf(format string, args ...interface{}) *FloatAsker {
	return &FloatAsker{
		sa: Stringf(format, args...),
	}
}

// BoolAsker is an object that can ask for a bool on the command line.
type BoolAsker struct {
	sa *StringAsker
}

// Ask prompts the user by displaying the BoolAsker's message, reads from standard input until it encounters a newline
// or EOF, converts the input to a bool and returns the bool. If the user enters an empty input and a default
// bool is set, the default int will be returned.
// Returns an error if stdin could not be read, if the input is too large for memory, or if the input could not be
// converted to bool.
//
// Case-insensitive inputs "y" and "yes" convert to true, "n" and "no" convert to false.
func (ba *BoolAsker) Ask() (bool, error) {
	s, err := ba.sa.doAsk(ba.getPrompt())
	if err != nil {
		return false, err
	}
	s = strings.ToLower(s)
	if s == "y" || s == "yes" {
		return true, nil
	} else if s == "n" || s == "no" {
		return false, nil
	}

	return false, fmt.Errorf("ask: error parsing bool from stdin: %#v is not in [y|n|yes|no]", s)
}

func (ba *BoolAsker) getPrompt() string {
	var b strings.Builder
	fmt.Fprintf(&b, ba.sa.format+" ", ba.sa.args...)
	if ba.sa.hasDefault {
		if ba.sa.defaultString == "y" {
			fmt.Fprint(&b, "[Y/n] ")
		} else {
			fmt.Fprint(&b, "[y/N] ")
		}
	}
	return b.String()
}

// Default sets the default bool for the BoolAsker. The default bool is returned from Ask if the user entered
// an empty input.
func (ba *BoolAsker) Default(def bool) *BoolAsker {
	if def {
		ba.sa = ba.sa.Default("y")
	} else {
		ba.sa = ba.sa.Default("n")
	}
	return ba
}

// Bool returns a BoolAsker that will prompt the user with message when Ask is called.
func Bool(message string) *BoolAsker { return Boolf(message) }

// Boolf returns a BoolAsker that will prompt the user with a message according to a format specifier. See the fmt
// package for formatting rules.
func Boolf(format string, args ...interface{}) *BoolAsker {
	return &BoolAsker{
		sa: Stringf(format, args...),
	}
}

// PathAsker is an object that can ask for a path on the command line.
type PathAsker struct {
	sa *StringAsker
}

// Ask prompts the user by displaying the PathAsker's message, reads from standard input until it encounters a newline
// or EOF and returns the input path. If the user enters an empty input and a default path is set, the default path will
// be returned. The user can auto-complete the path using TAB in the bash style.
// Returns an error if stdin could not be read, or if the input is too large for memory.
func (pa *PathAsker) Ask() (string, error) {
	return pa.sa.Ask()
}

// Default sets the default path for the PathAsker. The default path is returned from Ask if the user entered
// an empty input.
func (pa *PathAsker) Default(def string) *PathAsker {
	pa.sa.Default(def)
	return pa
}

// Path returns a PathAsker that will prompt the user with message when Ask is called.
func Path(message string) *PathAsker { return Pathf(message) }

// Pathf returns a PathAsker that will prompt the user with a message according to a format specifier. See the fmt
// package for formatting rules.
func Pathf(format string, args ...interface{}) *PathAsker {
	pa := &PathAsker{
		sa: Stringf(format, args...),
	}

	// Path completion func override
	pa.sa.completer = func(line string) []string {
		var err error
		line, err = homedir.Expand(line)
		if err != nil {
			return []string{}
		}
		files, err := ioutil.ReadDir(filepath.Dir(line))
		if err != nil {
			return []string{}
		}
		completions := make([]string, 0, len(files))
		_, prefix := filepath.Split(line)
		for _, f := range files {
			if strings.HasPrefix(f.Name(), prefix) {
				completions = append(completions, filepath.Join(filepath.Dir(line), f.Name()))
			}
		}
		return completions
	}

	return pa
}
