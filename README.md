# Ask

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/loderunner/ask/main)](https://gitlab.com/loderunner/ask/-/pipelines)
[![Go package reference](https://img.shields.io/badge/Go-reference-007d9c)](https://pkg.go.dev/gitlab.com/loderunner/ask)
![Go package version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=%24.Version&url=https%3A%2F%2Fproxy.golang.org%2Fgitlab.com%2Floderunner%2Fask%2F%40latest)

Ask is a library to create interactive prompts in the terminal using Go. Create
prompts, forms and dialogs in the command line.

```go
func main() {
	b, err := ask.Bool("Are you sure?").Default(true).Ask()

	if err != nil {
		panic(err)
	}

	if !b {
		fmt.Println("No.")
		os.Exit(1)
	}

	fmt.Println("Yes.")
}
```

![Ask animated demo](demo.gif)

## Features

- Read input from terminal and return a typed value
- Restrict valid input to a finite set of choices
- Auto-complete (for paths and choices) with tab
- Hide password input

## Install

```shell
go get gitlab.com/loderunner/ask
```

## Usage

To prompt the user for a value, create one of the typed "Askers" and use
`Ask`.

```go
asker := ask.String("Enter your name:")
name, err := asker.Ask()
```

or chain the calls for a simpler syntax:

```go
name, err := ask.String("Enter your name:").Ask()
```

`Ask` prints out the prompt given as argument, and waits for user input. Input
is terminated when the user ends the line by pressing Return. `Ask` then
returns the value entered by the user in the terminal. `err` is set if there was
an error reading from the terminal, or if the user-entered value could not be
coerced to the Asker's type (see below).

### Formatting prompt

The Asker's prompt can be customized further by using the `ask.*f` functions.
These are just shortcuts to Go's [`fmt`](https://golang.org/pkg/fmt/) package.
I.e.

```go
ask.Stringf("Player #%d input:", playerNumber).Ask()
```

is a shortcut for

```go
prompt := fmt.Sprintf("Player #%d input:", playerNumber)
ask.Stringf(prompt).Ask()
```

Here is an example using `Intf`.

```go
hobbits := []string{"Frodo", "Sam", "Merry", "Pippin"}
for _, name := range hobbits {
	age, err := ask.Intf("How old is %s?", name).Ask()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%d is such a nice age to go adventuring for a hobbit!\n", age)
}
```

### Configuring Askers

Once the asker has been created, it can be configured by calling a number of
methods, before finally calling `Ask` to collect user input. Configuration
options can change the prompt behavior, the input validation or the default
value (see below).

Configuration methods return the configured Asker, making it easy to chain
method calls, to configure and prompt user in a single line of code.

```go
meals := []string{
	"Breakfast",
	"Second Breakfast",
	"Elevenses",
	"Luncheon",
	"Afternoon Tea",
	"Dinner",
	"Supper",
}
favoriteMeal, err := ask.String("Which meal is your favorite?").
	Choices(meals...).
	Default("Second Breakfast").
	Ask()
```

### Default values

Askers can take a default value using `Default`. `Ask` will return the
default value if no input was given. If your asker has a default value, it will
be displayed after the custom prompt.

```go
dwarfCount, _ := ask.Int("How many dwarves should go reclaim Lonely Mountain?").
	Default(13).
	Ask()
if dwarfCount >= 10 {
	fmt.Printf("%d is a lot of dwarves.\n", dwarfCount)
}
```

Output:

```
How many dwarves should go reclaim Lonely Mountain? [13]
	<User presses Return without entering a value>
13 is a lot of dwarves
```

### Restricting choices

It can be useful to restrict the possible choices to a prompt, to a finite set
of values. `Choices` allows configuring the Asker to prompt for a set of choices
and will return an error if the user enters a value outside of these choices.

```go
what, _ := ask.String("What have the orcs taken to Isengard?").
	Choices("orcs", "hobbits", "elves").
	Ask()
fmt.Printf("They've taken the %s to Isengard!", what)
```

Output:

```
What have the orcs taken to Isengard? [orcs/hobbits/elves] hobbits
They've taken the hobbits to Isengard!
```

:warning: Using `Default` to set a default value not in the restricted choices
will result in undefined behavior.

### Password input

Using `Password` on a `StringAsker` will the input when the user types their
input.

### Tab completion

### Asker types

#### String

Using `String` or `Stringf` will create a `StringAsker`. `StringAsker.Ask`
expects any string and returns it.

`StringAsker` supports `Default`, `Choices` and `Password`.

#### Int

Using `Int` or `Intf` will create an `IntAsker`. `IntAsker.Ask` expects the user
to enter a text that can be parsed with [`strconv.Atoi`](https://golang.org/pkg/strconv/#Atoi)
and returns an `int`.

`IntAsker` supports `Default` and `Choices`.

#### Float

Using `Float` or `Floatf` will create a `FloatAsker`. `FloatAsker.Ask` expects
the user to enter a text that can be parsed with [`strconv.ParseFloat`](https://golang.org/pkg/strconv/#ParseFloat)
and returns a `float64`.

`FloatAsker` supports `Default` and `Choices`.

#### Boolean

Using `Bool` or `Boolf` will create a `BoolAsker`. `BoolAsker.Ask` expects the
user to enter either `yes` or `no`, or the shorthands `y` or `n`. `yes` or `y`
will return `true`, `no` or `n` will return `false`.

`BoolAsker` supports `Default`.

#### Path

Using `Path` or `Pathf` will create a `PathAsker`. `PathAsker.Ask` expects the
user to enter any string and returns it. `PathAsker` behaves just like
`StringAsker`, except for the tab completion which resolves the given path to
find possible completions.

`PathAsker` supports `Default`.

### Errors

`Ask` will returns error in two cases:

- failure to read input
- failure to parse input (or coerce it to restricted choices)

Errors use Go 1.13 error wrapping. Use [`errors.Unwrap`](https://golang.org/pkg/errors/#Unwrap)
to find the underlying error (if there is one).

## License

Copyright 2017 Pantomath SAS
Copyright 2021 Charles Francoise

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
