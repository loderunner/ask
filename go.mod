module gitlab.com/loderunner/ask

go 1.13

require (
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/peterh/liner v1.2.1
	github.com/rivo/uniseg v0.2.0 // indirect
)
