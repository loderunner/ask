// Copyright 2017 Pantomath SAS
// Copyright 2021 Charles Francoise
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ask

import (
	"os"
	"testing"
)

var osStdin, osStdout *os.File
var in, out *os.File

const testMessage = "Hello what?"
const testFormat = "Hello %s?"
const testArg = "what"
const testString = "Hello world!"
const testIntStr = "1138"
const testInt = 1138
const testFloatStr = "1.01"
const testFloat = 1.01

func hijack() {
	var err error
	osStdin = os.Stdin
	os.Stdin, in, err = os.Pipe()
	if err != nil {
		panic(err)
	}
	osStdout = os.Stdout
	out, os.Stdout, err = os.Pipe()
	if err != nil {
		panic(err)
	}
}

func restore() {
	os.Stdin = osStdin
	os.Stdout = osStdout
}

func TestString(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testString + "\n")
	if err != nil {
		panic(err)
	}

	s, err := String(testMessage).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if s != testString {
		t.Fatalf("expected %#v, got %#v", testString, s)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestStringf(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testString + "\n")
	if err != nil {
		panic(err)
	}

	s, err := Stringf(testFormat, testArg).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if s != testString {
		t.Fatalf("expected %#v, got %#v", testString, s)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestStringDefault(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString("\n")
	if err != nil {
		panic(err)
	}

	s, err := String(testMessage).Default(testString).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if s != testString {
		t.Fatalf("expected %#v, got %#v", testString, s)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " [" + testString + "] "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestStringChoices(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testString + "\n")
	if err != nil {
		panic(err)
	}

	s, err := String(testMessage).Choices(testString, "hello", "world").Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if s != testString {
		t.Fatalf("expected %#v, got %#v", testString, s)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " [" + testString + "/hello/world] "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestStringChoicesDefault(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString("\n")
	if err != nil {
		panic(err)
	}

	s, err := String(testMessage).
		Choices(testString, "hello", "world").
		Default(testString).
		Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if s != testString {
		t.Fatalf("expected %#v, got %#v", testString, s)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " [" + testString + "/hello/world] (default=" + testString + ") "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestInt(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testIntStr + "\n")
	if err != nil {
		panic(err)
	}

	i, err := Int(testMessage).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if i != testInt {
		t.Fatalf("expected %#v, got %#v", testInt, i)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestIntf(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testIntStr + "\n")
	if err != nil {
		panic(err)
	}

	i, err := Intf(testFormat, testArg).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if i != testInt {
		t.Fatalf("expected %#v, got %#v", testInt, i)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestFloat(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString(testFloatStr + "\n")
	if err != nil {
		panic(err)
	}

	f, err := Float(testMessage).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if f != testFloat {
		t.Fatalf("expected %#v, got %#v", testFloat, f)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}

func TestBool(t *testing.T) {
	hijack()
	defer restore()

	_, err := in.WriteString("y\n")
	if err != nil {
		panic(err)
	}

	yes, err := Bool(testMessage).Ask()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if !yes {
		t.Fatalf("expected %#v, got %#v", true, yes)
	}

	var b [256]byte
	n, err := out.Read(b[:])
	if err != nil {
		panic(err)
	}
	got := string(b[:n])
	expect := testMessage + " "
	if got != expect {
		t.Fatalf("expected output: %#v, got: %#v", expect, got)
	}
}
