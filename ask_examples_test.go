package ask_test

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	"gitlab.com/loderunner/ask"
)

func ExampleBool() {
	b, err := ask.Bool("Are you sure?").Default(true).Ask()
	if err != nil {
		panic(err)
	}
	if !b {
		fmt.Println("No.")
		os.Exit(1)
	}
	fmt.Println("Yes.")
}

func ExamplePath() {
	s, err := ask.Path("Enter path:").
		Ask()
	if err != nil {
		panic(err)
	}
	abs, err := filepath.Abs(s)
	if err != nil {
		panic(err)
	}
	fmt.Println(abs)
}

func ExampleStringAsker_Choices() {
	s, err := ask.String("What time of day is it?").
		Choices("morning", "midday", "afternoon", "evening", "night").
		Ask()
	if err != nil {
		panic(err)
	}
	fmt.Println("Good", s, "!")
}

func ExamplePathAsker_Default() {
	dir, err := homedir.Dir()
	if err != nil {
		panic(err)
	}
	dir = filepath.Join(dir, ".ask")
	dir, err = ask.Path("Enter configuration directory:").Default(dir).Ask()
	if err != nil {
		panic(err)
	}
	configPath := filepath.Join(dir, "config.yml")
	fmt.Printf("Configuration will be saved at %s\n", configPath)
}
