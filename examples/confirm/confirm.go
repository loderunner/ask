// Copyright 2017 Pantomath SAS
// Copyright 2021 Charles Francoise
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"

	"gitlab.com/loderunner/ask"
)

func main() {
	b, err := ask.Bool("Are you sure?").Default(true).Ask()
	if err != nil {
		panic(err)
	}
	if !b {
		fmt.Println("No.")
		os.Exit(1)
	}
	fmt.Println("Yes.")
}
